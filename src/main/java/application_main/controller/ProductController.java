package application_main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import application_main.Product;

@RestController
public class ProductController {
	@Autowired
	Product product;
	@GetMapping(path="api/Product")
	public Product getProduct() {
		product.productName="Pen";
		product.price=(double) 5;
		product.Quantity=100;
		product.category.name="Stationary";
		product.variations.variationList.add("Red color");
		product.variations.variationList.add("Yellow color");
		return product;
		
	}
	

}
