package application_main;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class Category {
 
	public String name;
	public String toString () {
		return "Category[name="+name+"]";
	}
}
